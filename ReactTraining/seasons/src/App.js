import React from 'react';
import logo from './logo.svg';
import './App.css';
import SeasonDisplay from './SeasonDisplay';

class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {lat: null, lon: null, errorMessage: ""};

  }
  componentDidMount(){
    window.navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position); 
        this.setState({lat: position.coords.latitude, lon: position.coords.longitude})
      },
      (error) => {
        this.setState({errorMessage: error.message});
      }
    );
  }
    
  render(){
   if(this.state.errorMessage && !this.state.lat){
      return <div className="App">Error: {this.state.errorMessage}</div>
   }
   else if(!this.state.errorMessage && this.state.lat){
     return <div className="App"><SeasonDisplay lat={this.state.lat}/></div>
   }

   else{
      return <div className="App">Loading...</div>
   }
  }
}

export default App;
