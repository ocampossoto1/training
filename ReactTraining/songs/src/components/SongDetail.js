import React from 'react';
import {connect} from 'react-redux';


const SongDetail = ({song}) => {
    if(!song){
        return <div>slect a song</div>
    }
    return <div>
        <div>
            <h3>Details For: </h3>
            <p> 
                Title: {song.title}
                <br/>
                Duration: {song.duration}
            </p>
            
        </div>
    </div>
}

const mapStateToProps = (state) => {
   return {song : state.selectedSong}
}


export default connect(mapStateToProps)(SongDetail);