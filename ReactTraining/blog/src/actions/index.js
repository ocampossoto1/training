import jsonPlacHolder from '../apis/jsonPlaceHolder';
import _ from 'lodash';


export const fetchPostAndUsers = () => async (dispatch, getState) =>{
    await dispatch(fetchPost());
    const userIds = _.uniq(_.map(getState().posts, 'userId')); 
    console.log(userIds);
    userIds.forEach( id => dispatch(fetchUser(id)));
};


export const fetchPost = () => 
   async dispatch => {
    const response = await jsonPlacHolder.get('/posts');
    dispatch( {
        type: 'FETCH_POSTS',
        payload: response.data
    });
   
}; 

export const fetchUser =(id) => async dispatch => {

    const response = await jsonPlacHolder.get(`/users/${id}`);
    dispatch({
        type: "FETCH_USER",
        payload: response.data
    });

};

// export const fetchUser =(id) => dispatch => _fetchUser(id, dispatch);
// const _fetchUser =  _.memoize(async(id, dispatch)=>{
//     const response = await jsonPlacHolder.get(`/users/${id}`);
//     dispatch({
//         type: "FETCH_USER",
//         payload: response.data
//     });

// })