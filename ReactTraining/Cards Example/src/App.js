import React from 'react';
import logo from './logo.svg';
import './App.css';
import faker from "faker"
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

function App() {
  return (
      <div className="ui container comments">

          <ApprovalCard>
              <CommentDetail author={faker.name.firstName()} timeAgo={faker.date.past()} avatar={faker.image.avatar()} comment={faker.lorem.text()} />
          </ApprovalCard>
          <ApprovalCard>
              <CommentDetail author={faker.name.firstName()} timeAgo={faker.date.past()} avatar={faker.image.avatar()} comment={faker.lorem.text()} />
          </ApprovalCard>
          <ApprovalCard>
            <CommentDetail author={faker.name.firstName()} timeAgo={faker.date.past()} avatar={faker.image.avatar()} comment={faker.lorem.text()} />
          </ApprovalCard>
    </div>
  );
}

export default App;
