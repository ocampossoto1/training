import React from 'react';

const CommentDetail = (props) => {
    var date = new Date(props.timeAgo);
    var dateShow = (date.getMonth() + 1) + "/" + date.getDay() + "/" + date.getFullYear() +" "+ date.getHours() + ":" + date.getMinutes();
    return (
        <div className="comment">
        <a href="/" className="avatar">
            <img alt="avatar" src={props.avatar} />
        </a>
        <div className="content">
            <a href="/" className="author">
                {props.author}
            </a>
                <div className="metadata">
                    <span className="date">{dateShow}</span>
            </div>
                <div className="text">{props.comment}</div>
        </div>
    </div>);
}

export default CommentDetail;